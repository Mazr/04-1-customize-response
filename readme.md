# 作业要求

## 1 Definition of Done

* 必须非常严格的按照题目中的定义进行实现。
* 每一种 case 都必须至少包含一个独立的测试。
* 所有的测试必须都成功。
* 注意代码的坏味道。尤其是命名的规范性以及重复代码的问题。

## 2 请书写测试来验证如下的 case

### 2.1 没有返回值的 Response

首先请在 Controller 定义一个 API：*GET /api/no-return-value* 这个 API 在 method 定义上并没有返回值（返回值为 `void`），那么请问它的 Response 的 Status Code 默认情况下是多少呢？写测试证明这一点。

### 2.2 `@ResponseStatus` 标记。

请查阅资料使用 `@ResponseStatus` 标记为另一个 API *GET /api/no-return-value-with-annotation* 自定义 Status Code：204。（注意查阅一下 204 是什么意思哦）

### 2.3 直接返回字符串

请创建另一个 API：*GET /api/messages/{message}* 令其方法直接返回 `String` 类型的消息 `{message}`，请书写测试验证其响应的 `Content-Type`，以及 Status Code。还请验证其消息本身。

### 2.4 直接返回对象

请创建另一个 API *GET /api/message-objects/{message}* 令其方法返回一个 `Message` 类型的实例。且其 `value` 为 `{message}`

```java
class Message {
  private final String value;
  
  ...
}
```

请书写测试验证 Response 的 `Content-Type`，body 以及 Status Code。

### 2.5 直接返回对象 + @ResponseStatus 标记

请创建另一个 API *GET /api/message-objects-with-annotation/{message}* 和 2.4 一样返回一个 `Message` 类型的实例并且其 `value` 为 `{message}`。

请使用 @ResponseStatus 标记将 Status Code 自定义为 `202`。

### 2.3 使用 `ResponseEntity`

请创建另一个 API *GET /api/message-entities/{message}*，其 Response 定义如下：

|项目|说明|
|---|---|
|Status Code|200|
|Content-Type|`application/json`|
|Body|`{ "value": "{message}" }`|

除此之外，还需要包含一个名为 `X-Auth` 的 header，其值为 `me`。请书写测试验证以上的 Response。