package com.twuc.webApp;

import com.jayway.jsonpath.JsonPath;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultMatcher;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.*;

@SpringBootTest
@AutoConfigureMockMvc
public class ResponseControllerTest {
    @Autowired
    private MockMvc mockMvc;

    @Test
    void should_return_status() throws Exception {
        mockMvc.perform(get("/api/no-return-value"))
                .andExpect(status().is(200));
    }

    @Test
    void should_return_204_status() throws Exception {
        mockMvc.perform(get("/api/no-return-value-with-annotation"))
                .andExpect(status().is(204));
    }

    @Test
    void should_return_message() throws Exception{
        mockMvc.perform(get("/api/messages/hello"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith("text/plain"))
                .andExpect(content().string("hello"));
    }

    @Test
    void should_return_message_object_value() throws Exception {
        mockMvc.perform(get("/api/messages-objects/hello"))
                .andExpect(status().isOk())
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.value").value("hello"));

    }

    @Test
    void should_return_message_object_value_202_status() throws Exception {
        mockMvc.perform(get("/api/messages-objects/status/hello"))
                .andExpect(status().is(202))
                .andExpect(content().contentTypeCompatibleWith(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.value").value("hello"));

    }

    @Test
    void should_return_response_message_setting_by_myself() throws Exception {
        mockMvc.perform(get("/api/message-entities/hello"))
                .andExpect(status().is(200))
                .andExpect(content().contentType(MediaType.APPLICATION_JSON_UTF8_VALUE))
                .andExpect(jsonPath("$.value").value("hello"))
                .andExpect(header().string("X-Auth","me"));
    }
}
