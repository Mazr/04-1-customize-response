package com.twuc.webApp;

public class Message {
    private String value;

    public Message() {
    }

    public Message(String value) {
        this.value = value;
    }

    public String getValue() {
        return value;
    }
}
