package com.twuc.webApp;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

@RestController
public class ResponseController {
    @GetMapping("/api/no-return-value")
    public void hhh(){}

    @GetMapping("/api/no-return-value-with-annotation")
    @ResponseStatus(HttpStatus.NO_CONTENT)
    public void hello(){}

    @GetMapping("/api/messages/{message}")

    public String sayHello(@PathVariable String message) {
        return message;
    }

    @GetMapping("/api/messages-objects/{message}")
    public Message sayHelloByMessage(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/api/messages-objects/status/{message}")
    @ResponseStatus(HttpStatus.ACCEPTED)
    public Message sayHelloByMessageWithStatus(@PathVariable String message) {
        return new Message(message);
    }

    @GetMapping("/api/message-entities/{value}")
    public ResponseEntity<Message> getResponseEntity(@PathVariable String value) {
        return ResponseEntity.ok().header("X-Auth","me").body(new Message(value));
    }

}
